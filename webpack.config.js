const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const node_modules = __dirname + '/node_modules';
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var BrotliPlugin = require('brotli-webpack-plugin');  //ENABLE FOR BROTLI
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    ////// COMMENT MODE SETTING OUT WHEN DEPLOYING ON PROD.
    mode: 'production',
    ////

    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin({
            terserOptions: {
                output: {
                    comments: false,
                },
            },
            extractComments: false,
        })],
    },
    plugins: [
        new CompressionPlugin(),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ['*.js', '*.js.br', '*.css', '*.css.br'],
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css"
        }),
        new BrotliPlugin({
            asset: '[path].br[query]',
            test: /\.(js|css|html|svg)$/,
            threshold: 10240,
            minRatio: 0.8
        })
    ],
    entry: [
        './src/app.js',
        './src/scss/app.scss',
    ],

    watch: true,
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.s?css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options:{
                            plugins: function () {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        loader: 'sass-loader'
                    }
                ],
                sideEffects: true
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', {loose: true, modules: false}]
                    ],
                    compact: false,
                    sourceType: 'unambiguous'
                }
                // include: [
                //     resolve('fe-common/src'),
                //     resolve('fe-styleguide/src'),
                //     resolve('src'),
                //     resolve('test'),
                //     resolve('node_modules/webpack-dev-server/client')
                // ]
            },
            ////// IF YOU NEED INLINE JQUERY, UNCOMMENT BELOW OTHERWISE REMOVE
            // {
            //     test:   require.resolve('jquery'),
            //     use: [{
            //         loader: 'expose-loader',
            //         options: 'jQuery'
            //     },
            //         {
            //             loader: 'expose-loader',
            //             options: '$'
            //         }]
            // },
            /////////

            {test: /\.(png|woff|woff2|eot|ttf|svg|png|jpg|tiff|gif|jpeg)$/, loader: 'url-loader?limit=100000'},
        ]

    }
};
